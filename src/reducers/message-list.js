import * as types from '../constants/action-types';

const initialState = {
  loading: true,
};

export default function messageList(state = initialState, action) {
  switch (action.type) {
    case types.GET_MESSAGE_LIST:
      return {
        payload: action.payload,
        loading: false,
      };
    case types.ADD_MESSAGE:
      return {
        ...state,
        payload: [
          {
            id: new Date().getTime().toString(),
            ...action.newMessage,
          },
          ...state.payload,
        ],
      };

    default:
      return state;
  }
}
