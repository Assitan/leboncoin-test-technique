import { combineReducers } from 'redux';
import messageList from './message-list';

export default combineReducers({
  messageList,
});
