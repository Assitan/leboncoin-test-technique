import * as types from '../constants/action-types';

import messageList from './message-list';

const initialState = {
  loading: true,
};

describe('messageList reducer', () => {
  it('should return the initial state if action is whatever', () => {
    expect(messageList(initialState, 'whatever')).toEqual(initialState);
  });

  it('initial state should use the default value if not provided', () => {
    const nothing = undefined;

    expect(messageList(nothing, 'whatever')).toEqual(initialState);
  });

  it('should get the payload when action is GET_MESSAGE_LIST', () => {
    expect(
      messageList(
        {},
        {
          type: types.GET_MESSAGE_LIST,
          payload: [],
        },
      ),
    ).toEqual({
      loading: false,
      payload: [],
    });
  });

  it('should get the created message when action is ADD_MESSAGE', () => {
    expect(
      messageList(
        [{
          id: '16252',
          message: 'Test',
          confidentiality: 'private',
        }],
        [{
          type: types.ADD_MESSAGE,
        }],
      ),
    ).toEqual([
      {
        id: '16252',
        message: 'Test',
        confidentiality: 'private',
      },
    ]);
  });
});
