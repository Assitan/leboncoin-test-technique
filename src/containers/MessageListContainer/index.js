import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import readMessagesList from '../../actions/message';
import CreateMessage from '../../components/create-message';
import DisplayMessage from '../../components/display-message';

export default function MessageListContainer() {
  const dispatch = useDispatch();
  const messageList = useSelector((state) => state.messageList);

  useEffect(() => {
    dispatch(readMessagesList());
  }, [dispatch]);

  return (
    <section>
      <h1>Write your toughts</h1>
      <CreateMessage />
      <DisplayMessage messages={messageList} />
    </section>
  );
}
