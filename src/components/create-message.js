import React, { useReducer } from 'react';
import { useDispatch } from 'react-redux';
import CONFIDENTIALITY from '../constants/default-values';
import { addMessage } from '../actions/message';

export default function CreateMessage() {
  const dispatch = useDispatch();

  const initValues = {
    username: '',
    confidentiality: CONFIDENTIALITY,
    message: '',
  };

  const [messageInput, setMessageInput] = useReducer(
    (state, newState) => ({ ...state, ...newState }),
    initValues,
  );

  const disabled = messageInput.username.length && messageInput.message.length;

  const handleChange = (event) => {
    const { name, value } = event.target;

    setMessageInput({ [name]: value });
  };

  const handleSubmit = (event) => {
    if (event) {
      event.preventDefault();
    }
    dispatch(addMessage(messageInput));
    setMessageInput(initValues);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group username">
        <input
          type="text"
          placeholder="Username"
          name="username"
          value={messageInput.username}
          onChange={handleChange}
          aria-label="username"
        />
        <select
          name="confidentiality"
          value={messageInput.confidentiality}
          onChange={handleChange}
        >
          <option value="private">Private</option>
          <option value="public">Public</option>
        </select>
      </div>
      <div className="form-group">
        <textarea
          name="message"
          value={messageInput.message}
          onChange={handleChange}
          maxLength="500"
          aria-label="message"
          placeholder="Your message ..."
        />
      </div>
      <button type="submit" disabled={!disabled}>Send</button>
    </form>
  );
}
