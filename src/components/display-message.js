import React from 'react';
import PropTypes from 'prop-types';
import global from '../../public/icons/global.svg';
import lock from '../../public/icons/lock.svg';

export default function DisplayMessage({ messages }) {
  return (
    <div>
      {messages.loading && (
      <article className="placeholder">
        <div />
        <div />
      </article>
      )}
      {messages.payload && messages.payload.map(({
        id, username, confidentiality, message,
      }) => (
        <article key={id} className="messages">
          <div>
            <b>{username}</b>
            <img src={confidentiality === 'private' ? lock : global} alt="Confidentiality icon" />
          </div>
          <p>{message}</p>
        </article>
      ))}
    </div>
  );
}

DisplayMessage.propTypes = {
  messages: PropTypes.shape({
    loading: PropTypes.bool,
    payload: PropTypes.instanceOf(Array),
  }),
};

DisplayMessage.defaultProps = {
  messages: PropTypes.shape({
    payload: [],
  }),
};
