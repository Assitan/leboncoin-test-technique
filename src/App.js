import React from 'react';
import MessageListContainer from './containers/MessageListContainer';
import './App.css';

export default function App() {
  return (
    <div className="app">
      <MessageListContainer />
    </div>
  );
}
