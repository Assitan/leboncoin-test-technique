import * as types from '../constants/action-types';
import message from '../constants/messages-list.json';

/**
 * Returns the messages list
 *
 * @return {Array}
 */

export default function readMessagesList() {
  return {
    type: types.GET_MESSAGE_LIST,
    payload: message,
  };
}

/**
 * Returns the created message
 *
 * @param {Object} newMessage message sent by the user
 *
 * @return {Object}
 */

export function addMessage(newMessage) {
  return {
    type: types.ADD_MESSAGE,
    newMessage,
  };
}
