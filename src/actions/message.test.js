import * as types from '../constants/action-types';
import readMessagesList, { addMessage } from './message';

describe('readMessagesList', () => {
  it('should return an action with type GET_MESSAGE_LIST', () => {
    const given = [
      {
        confidentiality: 'public',
        id: '63G4',
        message: 'Hello, what\'s new?',
        username: 'John',
      },
      {
        confidentiality: 'public',
        id: '63G69',
        message: 'I have an idea. Now, I\'m looking for a specific object.',
        username: 'Nora',
      },
      {
        confidentiality: 'private',
        id: '6BN3G4',
        message: 'I\'m busy tonight ...',
        username: 'Stephan',
      },
    ];
    expect(readMessagesList()).toEqual({
      payload: given,
      type: 'GET_MESSAGE_LIST',
    });
  });
});

describe('addMessage', () => {
  it('should return an action with type ADD_MESSAGE', () => {
    const given = {
      id: '192827333',
      username: 'Emile',
      message: 'How are you?',
      confidentiality: 'private',
    };

    expect(addMessage(given)).toEqual({
      type: types.ADD_MESSAGE,
      newMessage: {
        id: '192827333',
        username: 'Emile',
        message: 'How are you?',
        confidentiality: 'private',
      },
    });
  });
});
